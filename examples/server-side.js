#!/usr/bin/env node

import JST from "../src/backend.js";

const jst = new JST();
console.log(await jst.render("helloworld", {
	text: "Hello, World!",
	unsafe_text: "<THIS>& THAT</THIS>",
	array: ["a", "b", "c", "d"],
	menu: {
		"home": "/home",
		"promotions": "/promotions/index.php",
		"about": "/about-us"
	}
}));
