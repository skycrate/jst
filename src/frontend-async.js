import {Client} from "./thread.js";

export default class {
	// TODO: this cannot be hardcoded....
	_worker = new Client("/js/worker.js");
	constructor(directory = '.', extension = '.jst') {
		this._worker.send("init", {
			directory,
			extension
		});
	}
	render(uri, model, callback) {
		this._worker.send("render", {
			uri, model
		}, callback);
		return this;
	}
	domify(uri, model, callback) {
		return this.render(uri, model, contents => {
			const el = document.createElement('div');
			el.innerHTML = contents;
			callback(el.childNodes);
		});
	}
}