export const EXT = {
	// All .js and .mjs files are treated as Worker script MODULES.
	"\\.m?js$": source => new Worker(source, {type:'module'}),
	// Any request whih starts with ws:// or wss:// (Web Socket)
	"^ws?s:": source => new WebSocket(source),
	// Everything else, just assume it's an EventSource...
	".": source => new EventSource(source),
};

const test = (exp, value) => new RegExp(exp).test(value);
const find = (table, value) => table[Object.keys(table).find(exp => test(exp, value))];
const init = source => find(EXT, source)(source);
const open = source => typeof source !== 'string' ? source : init(source);

const random = (a = 0, b = 1) => a + Math.random() * (b - a);
const random_int = (a, b) => Math.round(random(a, b));

const uuid_part = (
	size = 3,
	chunk = "000" + (random_int(46656, 66654) | 0)
) =>
	chunk.toString(36).slice(-size);

export const uuid = (_parts = 2, _output = "") =>
	_parts ? uuid(_parts - 1, _output + uuid_part()) : _output

export class Thread {
	_source;
	constructor(source) {
		source && this.open(source);
	}
	open(source) {
		this._source = open(source);
		return this;
	}
	bind(source) {
		return this.open(source);
	}
	connect(source) {
		return this.open(source);
	}
	send(...messages) {
		if (this._source.send)
			this._source.send(JSON.stringify(messages));
		else if (this._source.postMessage)
			this._source.postMessage(messages);
		return this;
	}
	listen(listener) {
		const source = this._source;
		(source.on || source.addEventListener)("message", e =>
			listener(this._source.close ? JSON.parse(e.data) : e.data));
		return this;
	}
	close() {
		this._source.terminate ? this._source.terminate() :
			this._source.close && this._source.close();
		return this;
	}
}

export class Pub extends Thread {
	constructor(source) {
		super(source);
	}
	send(channel, message) {
		super.send(channel, message);
	}
}

export class Sub extends Thread {
	constructor(source) {
		super(source);
	}
	listen(channel, listener) {
		return super.listen(([chnnl, msg]) =>
			// This is the simplest... 
			channel === chnnl && listener(msg));
	}
}

export class Client extends Thread {
	_requests = {};
	constructor(source) {
		super(source);
		this.listen((...parts) => {
			const id = parts.shift();
			let callback = this._requests[id];
			if (callback && delete this._request[id])
				callback(...parts);
		});
	}
	id(id = uuid()) {
		return this._requests[id] ? this.id() : id;
	}
	send(...parts) {
		const id = this.id();
		if (typeof parts[parts.length - 1] === "function")
			this._requests[id] = parts.pop();
		return super.send(id, ...parts);
	}
}

export class Server extends Thread {
	constructor(source) {
		super(source);
	}
	listen(listener) {
		return super.listen(([id, ...parts]) =>
			listener(...parts, msg => this.send(id, msg)));
	}
}

export class Router extends Server {
	constructor(source) {
		super(source);
	}
	listen(pattern, listener) {
		return super.listen((route, msg, reply) =>
			test(pattern, route) && listener(msg, reply));
	}
}