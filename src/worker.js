import JST from "/js/frontend.js";
import {Router} from "/js/thread.js";

let jst;
let router = new Router(self);
router
	.listen("init", (config, reply) => {
		jst = new JST(config.directory, config.extension);
		reply(true); // Just send back a bool for now.
	})
	.listen("render", (options, reply) => {
		reply(jst.render(options.uri, options.model));
	});
